<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'bouton_generer'    => "Generate rendering",
	'label_tester'      => "Test syntax",
	'titre_code_genere' => "Generated HTML code",
	'titre_rendu_html'  => "HTML rendering",
	'texte_defaut'      => "This is a sample text written with SPIP syntax. Text can easily be formatted using some common keayboard keys. I can then insist on an {{important word}} or add some {italic}.

Try here ! Sentences are simply written one after each other. To change paragraph, just leave one blank line.

{{{Here is a heading}}}

SPIP takes care of typography. If spaces are missing in my punctuation, SPIP automatically adds them! not to mention  «quotation marks»! Pretty cool, no?

{{{Another heading}}}

I can do lists as well, tables, quotations, footnotes, etc. Learn more with [online help->http://www.spip.net/aide/?aide=raccourcis&lang=en].",
);

?>
