<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'bouton_generer'    => "Générer le rendu",
	'label_tester'      => "Tester la syntaxe",
	'titre_code_genere' => "Code HTML généré",
	'titre_rendu_html'  => "Rendu HTML",
	'texte_defaut'      => "Ceci est un exemple de texte rédigé avec la syntaxe de SPIP. Elle permet de mettre en forme le texte, facilement, en utilisant certains caractères du clavier. Je peux ainsi insister sur un {{mot important}} ou ajouter {de l’italique}.

Essayez ici ! On écrit simplement les phrases l’une à la suite de l’autre. Pour changer de paragraphe, il suffit de laisser une ligne vide.

{{{Voici un intertitre}}}

SPIP soigne la typographie. Si j’oublie les espaces de ma ponctuation, SPIP les ajoute automatiquement! sans oublier les «guillemets»! Plutôt sympa, non?

{{{Autre intertitre}}}

Je peux aussi faire des listes, des tableaux, des citations, des notes de bas de pages, etc. Pour en savoir plus, [consultez l’aide->https://www.spip.net/aide/?aide=raccourcis].",
);

?>
