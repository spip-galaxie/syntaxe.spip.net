<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

function formulaires_tester_syntaxe_spip_charger_dist(){
	$defaut = _T('syntaxe_spip:texte_defaut');

	$contexte = array(
		'test' => $defaut,
		'_test' => $defaut,
	);
	return $contexte;
}

function formulaires_tester_syntaxe_spip_verifier_dist(){
	$erreurs = array(
		'_test' => _request('test'),
	);
	return $erreurs;
}
